import React, { Component } from 'react';
import { Result, Button } from 'antd';
import notfound from './notfound.module.less';
export default class NotFound extends Component {
  state = {
    time: 5,
  };
  componentDidMount() {
    this.timer = setInterval(() => {
      let { time } = this.state;
      console.log('notfound');
      this.setState(
        {
          time: --time,
        },
        () => {
          console.log(time, 'time');
          if (time <= 0) {
            //回到首页
            this.props.history.replace('/index');
          }
        }
      );
    }, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  handleBackHome = () => {
    this.props.history.replace('/index');
  };
  render() {
    const { time } = this.state;
    return (
      <div>
        <Result
          status="404"
          title="404"
          subTitle="页面都是，请返回首页"
          extra={
            <Button
              className={notfound.btns}
              onClick={this.handleBackHome}
              type="primary"
            >
              {time}秒自动返回首页
            </Button>
          }
        />
      </div>
    );
  }
}
