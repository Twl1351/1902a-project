import { Component } from 'react';
import { ThemeContext } from '@/utils/context';
const connect = (mapStateToProps, mapDispatchToProps) => {
  return (Com) => {
    return class extends Component {
      render() {
        return (
          <ThemeContext.Consumer>
            {({ state, dispatch }) => {
              console.log(state, '^^^^^^^'); //相当于仓库的值
              let res = mapStateToProps(state); //返回值就是{flag:state.flag}
              let res1 = mapDispatchToProps(dispatch);
              //返回值也是一个对象，这个对象里面放的是函数 {fn1:function(){},fn2:function(){}}
              return <Com {...res} {...res1} />;
            }}
          </ThemeContext.Consumer>
        );
      }
    };
  };
};

export default connect;
