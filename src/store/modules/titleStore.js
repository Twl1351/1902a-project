import { makeAutoObservable } from 'mobx';
class TitleStore {
  title = '八维创作平台';

  constructor() {
    makeAutoObservable(this);
  }
  setTitle(val) {
    this.title = val;
    document.title = this.title;
  }
}

export default new TitleStore();
