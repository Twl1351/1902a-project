// import numStore from "./modules/numStore"
// import titleStore from "./modules/titleStore"

let context = require.context('@/store/modules', false, /\.js$/);
console.dir(context.keys());
let data = context.keys().reduce((prev, cur) => {
  // console.log(cur, 'cur');
  let key = cur.match(/^\.\/(\w+)\.js$/)[1];
  let val = context(cur).default;
  // console.log(val);
  // console.log(key, '&&&&&&&');
  prev[key] = val;
  return prev;
}, {});

// data = {numStore,titleStore}
export default data;
