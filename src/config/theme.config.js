export const dark = {
  '@primary-color': '#000',
  '@text-color': '#fff',
};

export const light = {
  '@primary-color': '#fff',
  '@text-color': '#000',
};
