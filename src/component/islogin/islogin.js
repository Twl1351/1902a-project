import React, { Component } from 'react';

import { Redirect } from 'react-router-dom';
const IsLogin = (Com) => {
  return class Islogin extends Component {
    state = {
      flag: true,
    };
    componentDidMount() {
      this.setState({
        flag: localStorage.getItem('token'),
      });
    }
    render() {
      console.log(this.props, 'islogin');
      return this.state.flag ? (
        <Com {...this.props} />
      ) : (
        <Redirect to="/login" />
      );
    }
  };
};

export default IsLogin;
