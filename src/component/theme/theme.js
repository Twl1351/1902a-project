import React, { Component } from 'react';
import { ThemeContext } from '@/utils/context';
import connect from '@/utils/connect';
class Theme extends Component {
  handleClickColor = (flag) => {
    // this.setState(
    //   {
    //     flag: !this.state.flag,
    //   },
    //   () => {
    //     window.less.modifyVars(this.state.flag ? light : dark);
    //   }
    // );

    // flag:true  白色  false 黑色
    this.props.changeTheme(flag);
  };
  render() {
    const { flag } = this.props;
    console.log(this.props, 'themeprops');
    return (
      <button onClick={() => this.handleClickColor(!flag)}>
        {flag ? '白色' : '黑色'}
      </button>
    );
  }
}

export default connect(
  (state) => {
    return {
      flag: state.flag,
    };
  },
  (dispatch) => {
    return {
      changeTheme(flag) {
        dispatch({
          type: 'flag',
          flag,
        });
      },
      aa() {
        //   dispatch(getinit)
      },
    };
  }
)(Theme);
