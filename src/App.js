import './App.css';
import React, { Component } from 'react';
import './styles/common.less';
import RootRouter from './router/index.js';
import ErrorBoundary from '@/component/ErrorBoundary/ErrorBoundary';
import store from '@/store/index';
import { Provider } from 'mobx-react';
import { ThemeContext } from '@/utils/context';
import { dark, light } from '@/config/theme.config';

console.log(ThemeContext, 'ThemeContext%%%%%');

// console.log(import('./views/index/index'))
// function aa(){ consumer
//   new Promise(()=>{
//     console.log(1111)
//   })
// }
// aa();
// @withRouter

// function Test(Com) {
//   return class extends Component {
//     render() {
//       return <Com age={123} />;
//     }
//   };
// }

// @Test
class App extends Component {
  state = {
    flag: true,
  };
  dispatchFn = (action) => {
    //redux-thunk
    if (typeof action == 'function') {
      return action(this.dispatchFn);
    } else {
      console.log('else');
      this.setState(
        {
          [action.type]: action.flag,
        },
        () => {
          window.less.modifyVars(this.state.flag ? light : dark);
        }
      );
    }
  };
  render() {
    // console.log(this.props.age);
    return (
      <div className="container">
        <ErrorBoundary>
          <ThemeContext.Provider
            value={{ state: this.state, dispatch: this.dispatchFn }}
          >
            <Provider {...store}>
              <RootRouter />
            </Provider>
          </ThemeContext.Provider>
        </ErrorBoundary>
      </div>
    );
  }
}

export default App;
// export default App; withRouter(App)
